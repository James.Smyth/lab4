package datastructure;

import java.util.ArrayList;

import cellular.CellState;

public class CellGrid implements IGrid {

    public int cols;
    public int rows;
    CellState[][] cellgrid;


    public CellGrid(int rows, int columns, CellState initialState) {
        new ArrayList<CellGrid>();
        this.cols = columns;
        this.rows = rows;
        this.cellgrid = new CellState[rows][columns];
	}   

    @Override
    public int numRows() {
        return rows;
    }

    @Override
    public int numColumns() {
        return cols;
    }

    @Override
    public void set(int row, int column, CellState element) {
        if (row > rows || column > cols || row<0 || column<0){
            throw new IndexOutOfBoundsException("Index out of bounds");
        }
        cellgrid[row][column] = element;

        
    }

    @Override
    public CellState get(int row, int column) {
        if (row > rows|| column > cols|| row<0 || column<0){
            throw new IndexOutOfBoundsException("Index out of bounds");
        }
        return cellgrid[row][column];
    }

    @Override
    public IGrid copy() {
        CellGrid copyGrid = new CellGrid(this.rows, this.cols, CellState.DEAD);
        for (int row = 0; row<this.rows; row++) {
            for (int col = 0; col<this.cols; col++) {
                CellState element = this.get(row, col);
                copyGrid.set(row, col, element);
            }
        }
        return copyGrid;
    }
    
}
